# dtrx-py3

This is a fork of [dtrx](https://brettcsmith.org/2007/dtrx/) with support for python3. The code was converted using the 2to3-3.7 tool and may need some manual adjustments. The test suite remains unmodified and currently fails (it's worth noting it failed before the conversion and after the conversion more tests passed!). These failures will need to be examined.
